module.exports = (client, ip) => {

    const {ButtonBuilder, EmbedBuilder, ActionRowBuilder, ButtonStyle } = require('discord.js');
    const config = require('../botconfig/embed.json');
    const log = require('../functions/log.js');

    const fs = require("fs"); //this package is for reading files and getting their inputs

    const { exec } = require('child_process');
    client.on('interactionCreate', async interaction => {
        if (!interaction.isCommand()) return;
    
        const command = interaction.commandName;
    
        if (interaction.commandName === 'ping') {
            //reply with the amount of ping
            await interaction.reply(`Pong! ${client.ws.ping}ms`);
            console.log(`Pong! ${client.ws.ping}ms`);
            exec(`curl -X POST ${ip}"?led=on\"`, (err, stdout, stderr) => {
                if (err) {
                    console.error(err);
                    return;
                }
                //return the light to its original state after 2 seconds
                setTimeout(() => {
                    exec(`curl -X POST ${ip}"?led=off\"`, (err, stdout, stderr) => {
                        if (err) {
                            console.error(err);
                            return;
                        }
                    });
                }, 1000);
            });
        }
    
        else if (interaction.commandName === 'list') {

            const con = require('../database/connect.js');
            //get the wordlist from the database with their corresponding emotes
            con.query("SELECT * FROM tblwords INNER JOIN tblemotes ON fkemoteid = emoteid", function (err, result, fields) {
                if (err) throw err;
                //make a variable called list and set it to an empty string
                var list = "";
                //loop through the result and add the word and the emote to the list variable
                for (var i = 0; i < result.length; i++) {
                    list += result[i].word + " " + result[i].emote + "\n";
                }
                //reply with the list
                interaction.reply(list);
            });
        }
    
        else if (interaction.commandName === 'help') {
            
            const embed = new EmbedBuilder()
                .setThumbnail(client.user.displayAvatarURL())
                .setTitle("HELP MENU 🔰 Commands")
                .setDescription('List of commands')
                .addFields(
                    { name: 'ping', value: 'Replies with Pong!' },
                    { name: 'list', value: 'Replies with a list of reactions!' },
                    { name: 'help', value: 'Replies with a list of commands!' },
                    { name: 'control', value: 'Replies with options to control the Pico W' },
                )
                .setColor(config.color)
                .setTimestamp();
    
            await interaction.reply({ embeds: [embed] });
    
        }
    
        else if (interaction.commandName === 'control') {
            //make embed with buttons for the user to control the bot
            const embed = new EmbedBuilder()
                .setTitle('Control Panel')
                .setDescription('Use the buttons below to control the LED')
                .setColor(config.color)
                .setTimestamp();
    
            const row = new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('primary')
                        .setLabel('Blink')
                        .setStyle(ButtonStyle.Primary),
    
                    new ButtonBuilder()
                        .setCustomId('secondary')
                        .setLabel('Turn on (constant)')
                        .setStyle(ButtonStyle.Success),
    
                    new ButtonBuilder()
                        .setCustomId('tertiary')
                        .setLabel('Turn off')
                        .setStyle(ButtonStyle.Danger),
                );
    
            await interaction.reply({ content: 'Control LED of Pico W,', components: [row] });
    
            const filter = i => i.customId === 'primary' || i.customId === 'secondary' || i.customId === 'tertiary';
            const collector = interaction.channel.createMessageComponentCollector({ filter, time: 15000 });
            //a webserver is hosted on Raspberry Pico W with a small python script that controls the Onboard LED, this command can send a request to the local webserver to turn the LED on, off or blink
    
            collector.on('collect', async i => {
                if (i.customId === 'primary') {
                    await i.update({ content: 'Blinking!', components: [] });
                    exec(`curl -X POST ${ip}"?led=blink\"`, (err, stdout, stderr) => {
                        if (err) {
                            const blinkErr = "BLINK ERROR. Could not send request to Pico W";
                            //log using log.js function
                            log(blinkErr, client);
                            console.error(blinkErr);
                            //send a new message to the channel with the error message
                            interaction.channel.send(blinkErr);
                            return;
                        }
                        
                        //console.log(stdout);
    
                        //close the collector
                        collector.stop();
                    });
                }
    
                else if (i.customId === 'secondary') {
                    await i.update({ content: 'On!', components: [] });
                    const { exec } = require('child_process');
                    exec(`curl -X POST ${ip}"?led=on\"`, (err, stdout, stderr) => {
                        if (err) {
                            const onErr = "ON ERROR. Could not send request to Pico W";
                            log(onErr, client);
                            console.error(onErr);
                            interaction.channel.send(onErr);
                        }
                        //console.log(stdout);
    
                        //close the collector
                        collector.stop();
                    });
                }
    
                else if (i.customId === 'tertiary') {
                    await i.update({ content: 'Off!', components: [] });
                    //send a link to the pico w to turn off
                    const { exec } = require('child_process');
                    exec(`curl -X POST ${ip}"?led=off\"`, (err, stdout, stderr) => {
                        if (err) {
                            const offErr = "OFF ERROR. Could not send request to Pico W";
                            log(offErr, client);
                            console.error(offErr);
                            interaction.channel.send(offErr);
                        }
                        //console.log(stdout);
                        collector.stop();
                    }
                    );
                }
            });
    
        }
    
        //when someone uses a command, log it in the console and log which command they used
        console.log(`${interaction.user.tag} in #${interaction.channel.name} triggered ${command}`);

    });
}