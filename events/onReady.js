//create module.exports with client and fs as parameters
module.exports = (client, fs) => {


  client.on('ready', () => {

    fs.writeFile('log.txt', '', function (err) {
      if (err) throw err;
    });

    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setPresence({ activities: [{ name: 'JavaScript' }], status: 'online' });

    //require database/connect.js and pass the client to it
    require('../database/connect.js')(client);

    client.application.commands.cache.clear();

    //remove slash commands from the bot to avoid keeping old commands
    //comment this out after running it once and restart discord client to see effect.

    // rest.put(Routes.applicationCommands(clientId), { body: [] })
    // .then(() => console.log('Successfully deleted all application commands.'))
    // .catch(console.error);

    //clear log.txt file to avoid having a huge file
    fs.writeFile("../log.txt", "", function (err) {
      if (err) {
        return console.log(err);
      }
    });

    const { SlashCommandBuilder } = require('@discordjs/builders');

    //add a slash command for each command in commands.json and add it to the bot
    fs.readFile('./botconfig/commands.json', (err, data) => {
      if (err) throw err;
      if (err) {
        log(err);
      }
      let commands = JSON.parse(data);
      Object.keys(commands).forEach(command => {
        client.application.commands.create(new SlashCommandBuilder()
          .setName(command)
          .setDescription(commands[command].description)
        );
      });
    });
  });

}
