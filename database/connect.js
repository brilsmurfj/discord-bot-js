module.exports = (client) => {
    
    //create a connection to the database
    var mysql = require('mysql');
    var host = process.env.HOST;
    var user = process.env.USER;
    var password = process.env.PASSWORD;
    var database = process.env.DATABASE;

    var con = mysql.createConnection({
        host: host,
        user: user,
        password: password,
        database: database
    });

        //connect to the database
        con.connect(function (err) {
            if (err) throw err;
            console.log("Connected!");
            //make it so con is accessible in other files
            module.exports = con;
        });
}