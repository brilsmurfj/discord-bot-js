//make a simple discord bot that responds to ping with pong
const { Client, Discord, REST, Routes, Events,   GatewayIntentBits, ApplicationCommand, MessageActionRow, ButtonBuilder, EmbedBuilder, ActionRowBuilder, ButtonStyle } = require('discord.js');
//get token from .env file (make sure to add .env to .gitignore)
require('dotenv').config();

const token = process.env.TOKEN;
//get the ip from the .env file and put it in a variable. This is the ip of the server that the bot can connect to and send GET requests to (Ex: turn on a light with http://(ip)/"?led=on\")
const ip = process.env.IP;
const config = require('./botconfig/embed.json');
const reactions = require('./reactions.json');

const fs = require("fs"); //this package is for reading files and getting their inputs
const client = new Client({
    intents:
        [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.GuildMessages,
            GatewayIntentBits.MessageContent,
            GatewayIntentBits.GuildMembers,
            GatewayIntentBits.GuildPresences
        ]
    }
);

//require onReady.js
require('./events/onReady.js')(client, fs);

//require onMessageCreate.js
require('./events/onMessageCreate.js')(client, fs, reactions);

//require interactionCreate.js
require('./events/interactionCreate.js')(client, ip, config, reactions);


client.login(token);