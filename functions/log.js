//log errors to log.txt
module.exports = (err) => {
    const fs = require('fs');
    fs.appendFile('log.txt', `${new Date().toLocaleString()} - ${err}\n\n`, function (err) {
        if (err) throw err;
    });
}